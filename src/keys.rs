use std::{fs::File, ffi::CString, path::PathBuf, io::Write};

use wayland_client::{protocol::wl_keyboard::KeyState, QueueHandle};
use xkbcommon::xkb;

use crate::{state::CANDIDATES, drawing::draw_buffer};

use super::state::State;

const MODIFIER_STATE_SHIFT: u32 = 0x1;
// const MODIFIER_STATE_CAPS: u32 = 0x2;
// const MODIFIER_STATE_CONTROL: u32 = 0x4;
// const MODIFIER_STATE_ALT: u32 = 0x8;
// const MODIFIER_STATE_SUPER: u32 = 0x40;

const SHIFT: u32 = 42;
const BACK_SPACE: u32 = 14;
const SPACE: u32 = 57;
const ENTER: u32 = 28;
const ESC: u32 = 1;
const DELETE: u32 = 111;
const LEFT: u32 = 105;
const RIGHT: u32 = 106;
const DOWN: u32 = 108;
const UP: u32 = 103;
const TAB: u32 = 15;

pub fn key(state: &mut State, qh: &QueueHandle<State>) {
    let serial = state.key_event.serial;
    let key = state.key_event.key;
    let key_state = state.key_event.state;
    let time = state.key_event.time;
    if state.active {
        if state.popup_surface.is_some() {
            popup_key(state, serial, key, key_state);
        } else {
            default_key(state, serial, key, key_state, time, qh);
        }
    } else {
        let virtual_keyboard = state.virtual_keyboard.as_ref().unwrap();
        virtual_keyboard.key(time, key, key_state.into());
    }
}

fn default_key(
    state: &mut State,
    serial: u32,
    key: u32,
    key_state: KeyState,
    time: u32,
    qh: &QueueHandle<State>,
) {
    let virtual_keyboard = state.virtual_keyboard.as_ref().unwrap().clone();
    let input_method = state.input_method.as_ref().unwrap().clone();
    match key_state {
        KeyState::Pressed => {
            match key {
                SHIFT => {
                    // if state.modifiers.mods_depressed & MODIFIER_STATE_SHIFT != 0 {
                    //     state.chewing.shift_left();
                    //     state.shift_pressed = false;
                    // } else {
                    //     state.shift_pressed = true;
                    // }
                }
                BACK_SPACE => {
                    state.chewing.backspace();
                }
                SPACE => {
                    if state.modifiers.mods_depressed & MODIFIER_STATE_SHIFT != 0 {
                        state.chewing.shift_space();
                    } else {
                        state.chewing.space();
                    }
                }
                ENTER => {
                    let commit_string =
                        format!("{}{}", state.chewing.buffer(), state.chewing.bopomofo());
                    input_method.commit_string(commit_string);
                    state.chewing.enter();
                    input_method.commit(serial);
                    state.chewing.esc();
                }
                ESC => {
                    state.chewing.esc();
                }
                DELETE => {
                    state.chewing.del();
                }
                LEFT => {
                    state.chewing.left();
                }
                RIGHT => {
                    state.chewing.right();
                }
                DOWN => {
                    state.chewing.down();
                    // TODO: Fix list handling, either just one character or everything in one go
                    state.list = state
                        .chewing
                        .list()
                        .iter()
                        .map(|&s| String::from(s))
                        .collect();
                    if !state.list.is_empty() {
                        state.popup_surface = Some(input_method.get_input_popup_surface(
                            &state.surface.as_ref().unwrap(),
                            qh,
                            (),
                        ));
                    }
                    state.selected = (0, 0);
                    let set = if state.list.len() < CANDIDATES {
                        (0, state.list.len())
                    } else {
                        (0, CANDIDATES)
                    };
                    state.set = set;
                    draw_buffer(state);
                }
                TAB => {
                    state.chewing.tab();
                }
                _ => {
                    //TODO: Any modifier except shift(maybe more) shall pass through
                    let xkbstate = &state.xkb_state;

                    if let Some(keyval) = std::char::from_u32(xkbstate.key_get_utf32(key + 8)) {
                        println!("{}", keyval);
                        state.chewing.default(keyval);
                    } else {
                        virtual_keyboard.key(time, key, key_state as _);
                    }
                }
            }
            let preedit_string = format!("{}{}", state.chewing.buffer(), state.chewing.bopomofo());
            let mut len = preedit_string.len();
            // TODO: cursor handling before drawing
            if state.popup_surface.is_some() {
                len -= 1;
            }
            if len == 0 {
                virtual_keyboard.key(time, key, key_state as _);
            }
            input_method.set_preedit_string(preedit_string, len as _, len as _);
            input_method.commit(serial);
        }
        KeyState::Released => {
            let preedit_string = format!("{}{}", state.chewing.buffer(), state.chewing.bopomofo());
            let len = preedit_string.len();
            if len == 0 {
                virtual_keyboard.key(time, key, key_state as _);
            }
            // if state.shift_pressed {
            //     state.chewing.capslock();
            //     state.shift_pressed = false;
            // }
        }
        _ => {}
    }
}

fn popup_key(
    state: &mut State,
    serial: u32,
    key: u32,
    key_state: KeyState,
) {
    let input_method = state.input_method.as_ref().unwrap();
    match key_state {
        KeyState::Pressed => {
            match key {
                2..=11 =>{
                    state
                        .chewing
                        .choose_by_index((state.selected.0 * CANDIDATES + (key-2) as usize) as u8);
                    let preedit_string =
                        format!("{}{}", state.chewing.buffer(), state.chewing.bopomofo());
                    let len = preedit_string.len();
                    input_method.set_preedit_string(preedit_string, len as _, len as _);
                    input_method.commit(serial);
                    if let Some(popup_surface) = &state.popup_surface {
                        popup_surface.destroy();
                    }
                    state.popup_surface = None;
                }
                ENTER | SPACE => {
                    state
                        .chewing
                        .choose_by_index((state.selected.0 * CANDIDATES + state.selected.1) as u8);
                    let preedit_string =
                        format!("{}{}", state.chewing.buffer(), state.chewing.bopomofo());
                    let len = preedit_string.len();
                    input_method.set_preedit_string(preedit_string, len as _, len as _);
                    input_method.commit(serial);
                    if let Some(popup_surface) = &state.popup_surface {
                        popup_surface.destroy();
                    }
                    state.popup_surface = None;
                }
                ESC => {
                    if let Some(popup_surface) = &state.popup_surface {
                        popup_surface.destroy();
                    }
                    state.popup_surface = None;
                }
                LEFT => {
                    if state.selected.0 > 0 {
                        state.selected.0 -= 1;
                    }
                }
                RIGHT => {
                    if state.selected.0 < state.list.len() / CANDIDATES {
                        state.selected.0 += 1;
                        if state.selected.0 * CANDIDATES + state.selected.1 >= state.list.len() {
                            state.selected.1 = state.list.len() % CANDIDATES - 1;
                        }
                        if !state.list_expanded {
                            state.list_expanded = true;
                        }
                    }
                }
                UP => {
                    if state.selected.1 > 0 {
                        state.selected.1 -= 1;
                    }
                }
                DOWN => {
                    if state.selected.1 < CANDIDATES - 1 {
                        state.selected.1 += 1;
                    }
                    if state.selected.0 * CANDIDATES + state.selected.1 >= state.list.len() {
                        state.selected.1 = state.list.len() % CANDIDATES - 1;
                    }
                }
                _ => {
                    println!("{}", key);
                }
            }
            let set = if state.list_expanded {
                (0, state.list.len())
            } else if state.list.len() < CANDIDATES {
                (0, state.list.len())
            } else {
                (0, CANDIDATES)
            };
            state.set = set;
            draw_buffer(state);
        }
        KeyState::Released => {}
        _ => {}
    }
}

pub fn get_keymap_as_file(state: &mut State) -> (File, u32) {
    let keymap = state.xkb_state.get_keymap().get_as_string(xkb::KEYMAP_FORMAT_TEXT_V1);
    let keymap = CString::new(keymap).expect("Keymap should not contain interior nul bytes");
    let keymap = keymap.as_bytes_with_nul();
    let dir = std::env::var_os("XDG_RUNTIME_DIR")
        .map(PathBuf::from)
        .unwrap_or_else(std::env::temp_dir);
    let mut file = tempfile::tempfile_in(dir).expect("File could not be created!");
    file.write_all(keymap).unwrap();
    file.flush().unwrap();
    (file, keymap.len() as u32)
}