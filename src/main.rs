use calloop::{
    timer::{TimeoutAction, Timer},
    EventLoop,
};
use chewing_im::{keys, state::State};
use smithay_client_toolkit::event_loop::WaylandSource;
use wayland_client::Connection;

fn main() {
    let conn = Connection::connect_to_env().unwrap();

    let event_queue = conn.new_event_queue();
    let qhandle = event_queue.handle();

    let display = conn.display();
    display.get_registry(&qhandle, ());

    let mut state = State::init();

    let mut event_loop: EventLoop<State> =
        EventLoop::try_new().expect("Failed to initialize the event loop!");
    let handle = event_loop.handle();

    let wayland_source =
        WaylandSource::new(event_queue).expect("Failed to add wayland event queue to calloop!");
    wayland_source
        .insert(handle.clone())
        .expect("Failed to insert wayland source into calloop handle");

    event_loop
        .run(None, &mut state, |shared_data| {
            if shared_data.pressing {
                let timer = Timer::from_duration(shared_data.delay);
                handle
                    .insert_source(timer, |_deadline, _: &mut (), shared_data| {
                        keys::key(shared_data, &qhandle.clone());
                        TimeoutAction::ToDuration(shared_data.rate)
                    })
                    .expect("Timer failed");
            }
        })
        .expect("Error during event loop!");
}
